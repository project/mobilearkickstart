; praugmentedreality make file for d.o. usage
core = "7.x"
api = "2"

; +++++ Modules +++++

projects[admin_menu][version] = "3.x-dev"
projects[admin_menu][subdir] = "contrib"

projects[module_filter][version] = "2.x-dev"
projects[module_filter][subdir] = "contrib"

projects[ctools][version] = "1.3"
projects[ctools][subdir] = "contrib"

projects[profiler_builder][version] = "1.0"
projects[profiler_builder][subdir] = "contrib"

projects[features][version] = "2.0-rc3"
projects[features][subdir] = "contrib"

projects[addressfield][version] = "1.0-beta4"
projects[addressfield][subdir] = "contrib"

projects[geofield][version] = "1.2"
projects[geofield][subdir] = "contrib"

projects[entity][version] = "1.2"
projects[entity][subdir] = "contrib"

projects[geocoder][version] = "1.x-dev"
projects[geocoder][subdir] = "contrib"

projects[geophp][version] = "1.x-dev"
projects[geophp][subdir] = "contrib"

projects[libraries][version] = "2.x-dev"
projects[libraries][subdir] = "contrib"

projects[drupal_ar][version] = "0.1"
projects[drupal_ar][subdir] = "contrib"

projects[services][version] = "3.5"
projects[services][subdir] = "contrib"

projects[services][version] = "3.5"
projects[services][subdir] = "contrib"

