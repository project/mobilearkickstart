<?php
/**
 * @file
 * drupal_ar.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function drupal_ar_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_address'
  $field_bases['field_address'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_address',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'addressfield',
    'settings' => array(),
    'translatable' => 0,
    'type' => 'addressfield',
  );

  // Exported field_base: 'field_coordinates'
  $field_bases['field_coordinates'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_coordinates',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'geofield',
    'settings' => array(),
    'translatable' => 0,
    'type' => 'geofield',
  );

  // Exported field_base: 'field_info'
  $field_bases['field_info'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_info',
    'foreign keys' => array(
      'format' => array(
        'columns' => array(
          'format' => 'format',
        ),
        'table' => 'filter_format',
      ),
    ),
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(),
    'translatable' => 0,
    'type' => 'text_long',
  );

  return $field_bases;
}
